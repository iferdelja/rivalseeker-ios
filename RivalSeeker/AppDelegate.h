//
//  AppDelegate.h
//  RivalSeeker
//
//  Created by Ivan Ferdelja on 12/29/12.
//  Copyright (c) 2012 Ivan Ferdelja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
