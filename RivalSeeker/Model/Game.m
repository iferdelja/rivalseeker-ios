//
//  Game.m
//  RivalSeeker
//
//  Created by Ivan Ferdelja on 12/29/12.
//  Copyright (c) 2012 Ivan Ferdelja. All rights reserved.
//

#import "Game.h"

@implementation Game

@synthesize ID = _ID;
@synthesize state = _state;

-(Game*)initWithDict:(NSDictionary*)dict
{
    self = [super init];
    if (self) {
        self.ID = [dict objectForKey:@"id"];
        NSString* state = [dict objectForKey:@"state"];
        _state = [self gameStateForStateString:state];
    }
    return self;
}

-(GameState)gameStateForStateString:(NSString*)stateString
{
    if ([stateString isEqualToString:@"ready"]) {
        return GameState_Ready;
    } else if ([stateString isEqualToString:@"waiting_players_to_join"]) {
        return GameState_WaitingPlayersToJoin;
    } else if ([stateString isEqualToString:@"waiting_players_to_accept"]) {
        return GameState_WaitingPlayersToAccept;
    } else if ([stateString isEqualToString:@"cancelled"]) {
        return GameState_Cancelled;
    } else {
        NSAssert(true, @"Unknown state found");
        return -1;
    }
}

-(NSString*)gameStateAsString
{
    switch (self.state) {
        case GameState_Cancelled:
            return @"cancelled";
        case GameState_Ready:
            return @"ready";
        case GameState_WaitingPlayersToJoin:
            return @"waiting_players_to_join";
        case GameState_WaitingPlayersToAccept:
            return @"waiting_players_to_accept";
        default:
            break;
    }
}

@end
