//
//  Game.h
//  RivalSeeker
//
//  Created by Ivan Ferdelja on 12/29/12.
//  Copyright (c) 2012 Ivan Ferdelja. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum _GameState {
    GameState_WaitingPlayersToJoin,
    GameState_WaitingPlayersToAccept,
    GameState_Cancelled,
    GameState_Ready
} GameState;

@interface Game : NSObject

@property(nonatomic, strong) NSString* ID;
@property(nonatomic, readonly) GameState state;

-(Game*)initWithDict:(NSDictionary*)dict;
-(NSString*)gameStateAsString;

@end
