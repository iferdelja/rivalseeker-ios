//
//  ViewController.h
//  RivalSeeker
//
//  Created by Ivan Ferdelja on 12/29/12.
//  Copyright (c) 2012 Ivan Ferdelja. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *lookForRivalsButton;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIButton *acceptGameButton;
@property (weak, nonatomic) IBOutlet UILabel *acceptGameLabel;
@property (strong, nonatomic) IBOutlet UITableView *staticTableView;
@property (weak, nonatomic) IBOutlet UILabel *placeGameLabel;
@property (weak, nonatomic) IBOutlet UIButton *placeSelectButton;
@property (weak, nonatomic) IBOutlet UIButton *gameSelectButton;

@end
