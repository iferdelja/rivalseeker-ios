//
//  ViewController.m
//  RivalSeeker
//
//  Created by Ivan Ferdelja on 12/29/12.
//  Copyright (c) 2012 Ivan Ferdelja. All rights reserved.
//

#import "ViewController.h"
#import "AFJSONRequestOperation.h"
#import "Game.h"
#import <AudioToolbox/AudioServices.h>
#import "Flurry.h"

#define kAPIPollInterval 5
#define kGameAcceptInterval 30
#define kAPIBaseURL @"http://dev.fiveminutes.eu/rivalseeker"

#define FLURRY_EVENT_GAME_READY @"game_ready"
#define FLURRY_EVENT_GAME_CANCELLED @"game_cancelled"
#define FLURRY_EVENT_LOOK_FOR_OPPONENTS @"look"
#define FLURRY_EVENT_STOP_LOOKING_FOR_OPPONENTS @"stop_looking"

@interface ViewController ()
{
    @private
    BOOL _isAttemptingToPlay;
    NSString* _userID;
    BOOL _currentUserAcceptedGame;

    Game* _game;
    
    BOOL _isAcceptCountdownRunning;
    u_int16_t _acceptCountdownSeconds;
    
    BOOL _isPlaceGameLabelAnimationRunning;
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeUserID];
    
    self.messageLabel.text = @"";
    self.statusLabel.text = @"";
    self.acceptGameLabel.text = @"";
    self.placeGameLabel.text = @"";
    [self.acceptGameButton setEnabled:NO];
    [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
    [self.lookForRivalsButton setEnabled:YES];
    [self.acceptGameButton setHidden:YES];
    
    [self.staticTableView setDelegate:self];
    [self.staticTableView setDataSource:self];
    
    UIImage *buttonImage = [[UIImage imageNamed:@"blueButton.png"]
                            resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageHighlight = [[UIImage imageNamed:@"blueButtonHighlight.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    UIImage *buttonImageDisabled = [[UIImage imageNamed:@"greyButton.png"]
                                     resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
    [self.lookForRivalsButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.lookForRivalsButton setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    [self.lookForRivalsButton setBackgroundImage:buttonImageDisabled forState:UIControlStateDisabled];
    
    [self.acceptGameButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [self.acceptGameButton setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
    [self.acceptGameButton setBackgroundImage:buttonImageDisabled forState:UIControlStateDisabled];
}

- (IBAction)didPressLookForRivalsButton:(id)sender {

    _game = nil;
    
    if (_isAttemptingToPlay) {
        self.messageLabel.text = @"";
        self.statusLabel.text = @"";
        self.acceptGameLabel.text = @"";

        [self.acceptGameButton setHidden:YES];
        [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
        _isAttemptingToPlay = NO;
        
        [self doLeaveGame];
        [Flurry logEvent:FLURRY_EVENT_STOP_LOOKING_FOR_OPPONENTS];
        
    } else {
        self.messageLabel.text = @"";
        self.statusLabel.text = NSLocalizedString(@"WAITING_OPPONENTS", @"");
        [self.lookForRivalsButton setTitle:NSLocalizedString(@"STOP_LOOKING_FOR_OPPONENT", @"") forState:UIControlStateNormal];
        [self.acceptGameButton setHidden:YES];
        _isAttemptingToPlay = YES;
        
        [self doJoinGame];
        [Flurry logEvent:FLURRY_EVENT_LOOK_FOR_OPPONENTS];
    }
    
    _currentUserAcceptedGame = NO;
}

-(void)initializeUserID
{
    NSString* userID = [[NSUserDefaults standardUserDefaults] stringForKey:@"user-id"];
    if (userID == nil) {
        CFUUIDRef uid = CFUUIDCreate(nil);
        CFStringRef uidStringRef = CFUUIDCreateString(nil, uid);
        userID = (__bridge NSString*)uidStringRef;
        NSLog(@"Generated user id %@", userID);
        [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"user-id"];
    }
    _userID = userID;
}

-(NSData*)userJSONObject {
    NSMutableDictionary *userDict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                     _userID, @"id",
                                     nil];
    NSError* error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userDict options:0 error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    
    //NSLog(@"User object built %@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    return jsonData;
}

-(void)onAPIErrorOccured:(NSError*)error
{
    _statusLabel.text = NSLocalizedString(@"ERROR", @"");
    _messageLabel.text = error.localizedDescription;
    [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
    [self.lookForRivalsButton setEnabled:YES];
}

- (IBAction)confirmGame:(id)sender {
    
    _isAcceptCountdownRunning = NO;
    _currentUserAcceptedGame = YES;
    [self.acceptGameButton setEnabled:NO];
    [self.acceptGameButton setHidden:YES];
    self.statusLabel.text = @"Accepting game...";
    self.messageLabel.text = @"";
    self.acceptGameLabel.text = @"";
    [self.lookForRivalsButton setEnabled:NO];
    [self doAcceptGame];
}

-(void)updateGame:(Game*)game
{
    _game = game;
    NSLog(@"game updated, state: %@", [_game gameStateAsString]);
    switch (_game.state) {
        case GameState_Ready:
            {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                
                _statusLabel.text = NSLocalizedString(@"ALL_ACCEPTED", @"");
                _messageLabel.text = @"";
                [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
                [self.lookForRivalsButton setEnabled:YES];
                [self.acceptGameButton setHidden:YES];
                _isAttemptingToPlay = NO;
                
                [Flurry logEvent:FLURRY_EVENT_GAME_READY];
            }
            break;
            
        case GameState_WaitingPlayersToJoin:
            if (!_isAttemptingToPlay) {
                return;
            }
            
            self.messageLabel.text = @"";
            self.statusLabel.text = NSLocalizedString(@"WAITING_OPPONENTS", @"");
            [self.lookForRivalsButton setTitle:NSLocalizedString(@"STOP_LOOKING_FOR_OPPONENT", @"") forState:UIControlStateNormal];
            [self.lookForRivalsButton setEnabled:YES];
            [self scheduleGameStatusRefresh];
            _currentUserAcceptedGame = NO;

            break;
            
        case GameState_WaitingPlayersToAccept:
            
            if (_currentUserAcceptedGame) {
                self.messageLabel.text = @"";
                self.statusLabel.text = NSLocalizedString(@"WAITING_OPPONENT_ACCEPT", @"");
                [self.lookForRivalsButton setTitle:NSLocalizedString(@"STOP_LOOKING_FOR_OPPONENT", @"") forState:UIControlStateNormal];
                [self.lookForRivalsButton setEnabled:NO];
                [self.acceptGameButton setHidden:YES];
                [self scheduleGameStatusRefresh];
            } else {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                
                self.messageLabel.text = @"";
                self.statusLabel.text = NSLocalizedString(@"ACCEPT_GAME", @"");
                [self.lookForRivalsButton setTitle:NSLocalizedString(@"STOP_LOOKING_FOR_OPPONENT", @"") forState:UIControlStateNormal];
                [self.lookForRivalsButton setEnabled:NO];
                [self.acceptGameButton setHidden:NO];
                [self.acceptGameButton setEnabled:YES];

                _isAcceptCountdownRunning = YES;
                _acceptCountdownSeconds = kGameAcceptInterval;
                
                [self scheduleAcceptMessageTick];
            }
            break;
            
        case GameState_Cancelled:
        {
            _statusLabel.text = NSLocalizedString(@"GAME_CANCELLED", @"");
            _messageLabel.text = @"";
            [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
            [self.lookForRivalsButton setEnabled:YES];
            _isAttemptingToPlay = NO;
            
            [Flurry logEvent:FLURRY_EVENT_GAME_CANCELLED];
        }
            break;
            
        default:
            break;
    }
}

-(void)scheduleAcceptMessageTick
{
    if (_acceptCountdownSeconds == kGameAcceptInterval) {
        self.acceptGameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"GAME_AUTOCANCEL", @""), _acceptCountdownSeconds];
        _acceptCountdownSeconds--;
    }
    
    dispatch_time_t pingTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
    dispatch_after(pingTime, dispatch_get_main_queue(),
       ^(void){
           if (!_isAcceptCountdownRunning) {
               return;
           }
           
           self.acceptGameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"GAME_AUTOCANCEL", @""), _acceptCountdownSeconds];
           if (_acceptCountdownSeconds > 0) {
               _acceptCountdownSeconds--;
               [self scheduleAcceptMessageTick];
           } else {
               [self onAcceptCountdownExpired];
           }
               
       });
}

-(void)onAcceptCountdownExpired
{
    _isAcceptCountdownRunning = NO;
    _currentUserAcceptedGame = NO;
    _isAttemptingToPlay = NO;
    [self.acceptGameButton setEnabled:NO];
    self.statusLabel.text = @"";
    self.messageLabel.text = @"";
    self.acceptGameLabel.text = @"";
    [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
    [self.lookForRivalsButton setEnabled:NO];
    [self.acceptGameButton setHidden:YES];

    
}

-(void)onGameLeft
{
    self.messageLabel.text = @"";
    self.statusLabel.text = @"";
    self.acceptGameLabel.text = @"";
    [self.acceptGameButton setEnabled:NO];
    [self.lookForRivalsButton setEnabled:YES];
    [self.lookForRivalsButton setTitle:NSLocalizedString(@"FIND_OPPONENT", @"") forState:UIControlStateNormal];
    _isAttemptingToPlay = NO;
}

-(void)onGameLeavingFailed
{
    
}

-(void)scheduleGameStatusRefresh
{
    dispatch_time_t pingTime = dispatch_time(DISPATCH_TIME_NOW, kAPIPollInterval * NSEC_PER_SEC);
    dispatch_after(pingTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
        ^(void){
            [self doFetchGame];
        });
}

-(void)scheduleJoinGameAttempt
{
    dispatch_time_t pingTime = dispatch_time(DISPATCH_TIME_NOW, kAPIPollInterval * NSEC_PER_SEC);
    dispatch_after(pingTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
        ^(void){
            [self doJoinGame];
    });
}

-(void)executeGameRequest:(NSString*)urlString method:(NSString*)method body:(NSData*)bodyData
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = method;
    if (bodyData) {
        request.HTTPBody = bodyData;
    }
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSLog(@"%@", [request URL]);
    NSLog(@"%@", [request allHTTPHeaderFields]);
    if (bodyData) {
        NSLog(@"%@", [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding]);
    }
    //NSLog(@"%@",[request valueForHTTPHeaderField:field]);
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                         
        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if (!_isAttemptingToPlay) {
                return;
            }
            
            switch (response.statusCode) {
                case 200:
                {
                    NSDictionary* jsonDict = JSON;
                    Game* game = [[Game alloc] initWithDict:jsonDict];
                    [self updateGame:game];
                }
                default:
                    break;
            }
        }

        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError* error, id JSON) {
            NSLog(@"%@", [response URL]);
          
            NSLog(@"%@", [response allHeaderFields]);
            NSLog(@"failure status: %u  error: %@", response.statusCode, error);
            [self onAPIErrorOccured:error];
        }];
    
    [operation start];
}

-(void)doAcceptGame
{
    [self executeGameRequest:[NSString stringWithFormat:@"%@/acceptgame", kAPIBaseURL] method:@"POST" body:[self userJSONObject]];
}

-(void)doJoinGame
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/joingame", kAPIBaseURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = [self userJSONObject];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                         
        success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
            if (!_isAttemptingToPlay) {
                return;
            }
            
            switch (response.statusCode) {
                case 200:
                {
                    NSDictionary* jsonDict = JSON;
                    Game* game = [[Game alloc] initWithDict:jsonDict];
                    [self updateGame:game];
                }
                    break;
                case 204:
                    NSLog(@"Cannot join a game at this moment. ");
                    if (_isAttemptingToPlay) {
                        [self scheduleJoinGameAttempt];
                    }
                default:
                    break;
            }
        }

        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError* error, id JSON) {
            NSLog(@"failure status: %u  error: %@", response.statusCode, error);
            [self onAPIErrorOccured:error];
        }];
    
    [operation start];
}

-(void)doFetchGame
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/games", kAPIBaseURL]];
        NSError* error;
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self onAPIErrorOccured:error];
            });
            return;
        }
        
        id jsondata = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self onAPIErrorOccured:error];
            });
            return;
        }
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsondata options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self onAPIErrorOccured:error];
            });

        } else {
            NSDictionary* jsonDict = jsonObjects;
            Game* game = [[Game alloc] initWithDict:jsonDict];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateGame:game];
            });
        }
    });
}

-(void)doLeaveGame
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/leavegame", kAPIBaseURL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    request.HTTPBody = [self userJSONObject];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                         
    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"success %u", response.statusCode);
        if (response.statusCode == 200) {
            [self onGameLeft];
        }
    }

    failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError* error, id JSON) {
        if (response.statusCode == 406) {
            [self onGameLeavingFailed];
            return;
        }
        
        NSLog(@"failure status: %u  error: %@", response.statusCode, error);
        [self onAPIErrorOccured:error];
    }];
    
    [operation start];
    
}

- (IBAction)onPlaceSelectButtonPressed:(id)sender {
    self.placeGameLabel.text = NSLocalizedString(@"PLACE_FIXED", @"");
    dispatch_time_t pingTime = dispatch_time(DISPATCH_TIME_NOW, 2.5 * NSEC_PER_SEC);
    dispatch_after(pingTime, dispatch_get_main_queue(),
                   ^(void){
                       self.placeGameLabel.text = @"";
                   });
}

- (IBAction)onGameSelectButtonPressed:(id)sender {
    self.placeGameLabel.text = NSLocalizedString(@"GAME_FIXED", @"");
    dispatch_time_t pingTime = dispatch_time(DISPATCH_TIME_NOW, 2.5 * NSEC_PER_SEC);
    dispatch_after(pingTime, dispatch_get_main_queue(),
                   ^(void){
                       self.placeGameLabel.text = @"";
                   });
}

- (void)viewDidUnload {
    [self setLookForRivalsButton:nil];
    [self setMessageLabel:nil];
    [self setStatusLabel:nil];
    [self setAcceptGameButton:nil];
    [self setAcceptGameLabel:nil];
    [self setStaticTableView:nil];
    [self setPlaceGameLabel:nil];
    [self setPlaceSelectButton:nil];
    [self setGameSelectButton:nil];
    [super viewDidUnload];
}

@end
